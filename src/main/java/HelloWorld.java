
public class HelloWorld {
	public void printHelloWorld()
	{
		System.out.println("HelloWorld");
	}
	
	public String returnHelloWorld()
	{
		return "Hello World";
	}
	
	public void printCustomString(String str)
	{
		System.out.println(str);
	}
}
