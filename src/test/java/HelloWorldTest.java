import static org.junit.Assert.*;

import org.junit.BeforeClass;
import org.junit.Test;

public class HelloWorldTest {

	static HelloWorld hw = null;
	
	@BeforeClass
	public static void setUp()
	{
		hw = new HelloWorld();
	}
	
	@Test
	public void returnHelloWorldTest()
	{
		assertEquals("Hello World", hw.returnHelloWorld());
	}
}
